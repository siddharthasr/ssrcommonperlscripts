#!/usr/bin/perl

use strict ;
use warnings ;
use feature ":5.10" ;

use Env ;
use Env "DSNHOME" ;

my ${debugOption} = "-d" ;
my ${startOption} = "+s" ;
my ${stopOption} = "-s" ;

my ${DSNBackendServiceName} = "DSNBackendServer" ;

my %{serviceScriptLocations} = 
    (
    ${DSNBackendServiceName} => 
        {
        ${startOption} => "${DSNHOME}/startDSNBackendServer.pl",
	${stopOption} => "${DSNHOME}/home/bitnami/ssrana/DSNBackendServer/stopDSNBackendServer.pl"
        }
    ) ;

{
    printStartingDebugs() ;

    ## Check to see that there is something to handle
    if(0 > $#ARGV)
    {
        print "\nNo arguments to handle. Quitting.\n" ;
	exit(1) ;
    }

    my ${operationIdentifier} = ${ARGV}[0] ;

    given(${operationIdentifier})
    {
        when(${DSNBackendServiceName}) 
	{
	    print "\nHandling for ${DSNBackendServiceName} is requested. Proceeding to process call." ;
	    my @{handlingArgsArray} ;
	    if(1 <= $#ARGV)
	    {
	        print "\nCreating input args with real parameters ranging from 1 to $#ARGV" ;
	        @{handlingArgsArray} = @{ARGV}[1..$#ARGV] ;
            }
	    else
	    {
	        print "\nCreating dummy input args." ;
	        @{handlingArgsArray} = (undef) ;
	    }
	    print "\nhandlingArgsArray is : '" . join (",", @{handlingArgsArray}) . "'" ;
	    handleDSNService(\@{handlingArgsArray}) ;
	}
        default
	{
	    print "\nUnknown operationId (first argument). Aborting ...\n" ;
	    exit (0) ;
	}
    }
}

print "\n" ;


sub printStartingDebugs
{
    my $commandLineArgs = join ' ', @{ARGV} ;
    print "\nInput command line is :\n${commandLineArgs}" ;
}

sub printInputParams
{
    my @{inputParamsArray} = $_[0] ;
    for(my $loop1 = 0 ; $loop1 <= $#inputParamsArray ; $loop1++)
    {
        my $currentArg = ${inputParamsArray}[${loop1}] ;
        print "\nInput parameter ${loop1} is : '${currentArg}'" ;
    }
}

sub handleDSNService
{
    print "\nInput size for handleDSNService is : $#_" ;
    my @{inputArgs} = @{$_[0]} ;

    printInputParams(@{inputArgs}) ;

    my ${dsnWithDebug} = 0 ;
    my ${startOrStop} = ${startOption} ;


    ## Check to see that there is something to handle
    {
        no warnings ;

        if(! (defined ${inputArgs[0]}))
        {
    	    print "\nDSNBackendService operation not provided any arguments to process request. Aborting ...\n" ;
            exit(1) ;
        }
    }

    my @{scriptArgs} = () ;

    for(my $loop1 = 0 ; $loop1 <= $#inputArgs ; $loop1++)
    {
        my $currentArg = ${inputArgs}[${loop1}] ;
        print "\nHandling parameter : '${currentArg}'" ;
	given(${currentArg})
	{
	    when(${debugOption})
	    {
	        print "\nDebug option found for DSNServer." ;
	        ${dsnWithDebug} = 1 ;
		${scriptArgs}[0] = "-d" ;
	    }
	    when(${startOption})
	    {
	        print "\nStart option found for DSNServer." ;
		${startOrStop} = ${startOption} ;
	    }
	    when(${stopOption})
	    {
	        print "\nStop option found for DSNServer." ;
		${startOrStop} = ${stopOption} ;
	    }
	    default
	    {
	        print "\nUnknown argument passed to DSNBackendServer for handling. Aborting...\n" ;
		exit(1) ;
	    }
	}
    }

    print "\nprinting serviceScriptLocations content" ;
    foreach(sort keys %{serviceScriptLocations})
    {
        print "\n$_ : ${serviceScriptLocations{$_}}\n";
    }

    my ${scriptsHashReference} = ${serviceScriptLocations{${DSNBackendServiceName}}} ;
    print "\nscriptsHashReference = ${scriptsHashReference}" ;
    my %{scriptsHash} = %${scriptsHashReference} ;
    print "\nPrinting scriptsHash content" ;
    foreach(sort keys %{scriptsHash})
    {
        print "\n$_ : ${scriptsHash{$_}}\n";
    }
    my ${scriptToRun} = ${scriptsHash{${startOrStop}}} ;
    print "\nRunning DSNBackendServer script '${scriptToRun} " . join (" ", @{scriptArgs}) . "'";
    system(${scriptToRun}, @{scriptArgs}) ;
}

print "\n" ;
