#!/usr/bin/perl

use strict ;
use warnings ;

use Env ;
use Env "SSRPERLHOME" ;

print "\nValue of SSRPERLHOME is : ${SSRPERLHOME}" ;

use lib "${SSRPERLHOME}" ;

use SSRCommonPerl::ProcessHandling ;


{
    my $dsnProcessSearchString = "decentralizedsupplynetwork/backend" ;

    my $dsnProcessPid = SSRPerlScripts::CommonPerl::GetProcessPid(${dsnProcessSearchString}) ;
    {
        no warnings ;

	if(${dsnProcessPid} == "")
	{
	    print "\nThe process is not up right now. Nothing to do. Quitting ...\n" ;
	    exit(0) ;
	}
	else
	{
	    print "\nThe DSNBackend server process is up with the following pid : ${dsnProcessPid}" ;
	    print "\nHave to shut down the server." ;
	    SSRPerlScripts::CommonPerl::KillProcess(${dsnProcessSearchString}) ;
	}
    }
}

print "\n" ;
