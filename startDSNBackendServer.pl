#!/usr/bin/perl

use strict ;
use warnings ;

use Env ;
use Env "SSRPERLHOME" ;
use Env "DSNHOME" ;

print "\nValue of SSRPERLHOME is : ${SSRPERLHOME}" ;

use lib "${SSRPERLHOME}" ;

use SSRCommonPerl::ProcessHandling ;
use SSRCommonPerl::FileHandling ;

{

    my ${dsnStartCommand} = "" ;
    my ${debugFlag} = 0 ;
    my ${nohupFileExists} = 0 ;

    if(defined ${ARGV}[0])
    {
        if("-d" == ${ARGV}[0])
	{
	    print "Debug flag found in command line arguments for processing. Enabling debug mode." ;
	    ${debugFlag} = 1 ;
	}
	else
	{
	    my ${args} = join(" ", @{ARGV}) ;
	    print "\nUnknown command line arguments '${args}' passed. Ignoring." ;
	}
    }
    my ${dsnProcessPidSearchString} = "decentralizedsupplynetwork/backend" ;
    my ${dsnStartCommandNoDebug} = "nohup java -d64 -cp /home/bitnami/ssrana/DSNBackendServer:/home/bitnami/ssrana/devjars/mysql-connector-java-5.0.8/mysql-connector-java-5.0.8-bin.jar:/home/bitnami/ssrana/SSRCommonLibrary/SSRCommonLibrary.jar decentralizedsupplynetwork/backend/TestClass &" ;
    my ${dsnStartCommandWithDebug} = "nohup java -Xdebug -Xrunjdwp:transport=dt_socket,address=8800,server=y,suspend=y -d64 -cp /home/bitnami/ssrana/DSNBackendServer:/home/bitnami/ssrana/devjars/mysql-connector-java-5.0.8/mysql-connector-java-5.0.8-bin.jar:/home/bitnami/ssrana/SSRCommonLibrary/SSRCommonLibrary.jar decentralizedsupplynetwork/backend/TestClass &" ;

    print "\nThis is the start of startDSNBackendServer.pl script." ;
    SSRPerlScripts::CommonPerl::CheckFileHandlingModuleLoaded() ;
    SSRPerlScripts::CommonPerl::CheckProcessHandlingModuleLoaded() ;
    print "\nValue of the DSNHOME environment variable read is : ${DSNHOME})" ;
    my ($lastNohupFileName, $nextNohupFileName) = SSRPerlScripts::CommonPerl::FindLastAndNextNohupFileName(${DSNHOME}) ;
    print "\nValue of lastNohupFileName is : ${lastNohupFileName}" ;
    print "\nValue of nextNohupFileName is : ${nextNohupFileName}" ;
    if(-e "${DSNHOME}/nohup.out")
    {
        print "\nnohup.out file exists in the location ${DSNHOME}. It needs to be cleaned up first."
    }
    my $dsnProcessPid = SSRPerlScripts::CommonPerl::GetProcessPid(${dsnProcessPidSearchString}) ;
    {
        no warnings ;
	if(${dsnProcessPid} == "")
	{
	    print "\nDSNBackendServer is not running. No need to bring it down." ;
	}
	else
	{
	    print "\nDSNBackendServer is running. Must bring it down in order to start it. Nothing to do for startDSNBackenDServer.pl script. Quitting...\n" ;
	    exit(0) ;
	}
    }

    if(${debugFlag})
    {
        ${dsnStartCommand} = ${dsnStartCommandWithDebug} ;
    }
    else
    {
        ${dsnStartCommand} = ${dsnStartCommandNoDebug} ;
    }

    print "\nCommand to start DSNServer would be :\n${dsnStartCommand}" ;

    my ${nohupCheckLocation} = "${DSNHOME}/nohup.out" ;
    print "\nChecking for nohup.out file in : '${nohupCheckLocation}'" ;
    if(-e ${nohupCheckLocation})
    {
        print "\nnohup.out file already exists in the DSNHOME location. Moving current nohup.out to backup." ;
        my (${lastLogFile}, ${nextLogFile}) = SSRPerlScripts::CommonPerl::FindLastAndNextNohupFileName(${DSNHOME}) ;
        print "\nLast log file in DSNHOME is : '${lastLogFile}'" ;
        print "\nNext log file in DSNHOME is : '${nextLogFile}'" ;
	system("cd ${DSNHOME} ; mv nohup.out ${nextLogFile}") ;
    }
    system("cd ${DSNHOME} ; ${dsnStartCommand}") ;
    print "\nThis is the end of startDSNBackendServer.pl script." ;
}

print "\n" ;
