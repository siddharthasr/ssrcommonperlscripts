#!/usr/bin/perl

use strict ;
use warnings ;

package SSRPerlScripts::CommonPerl ;

sub FindNextNohupFileName
{
    my $pathNameToSearch = $_[0] ;
    my ($lastName , $nextName) = FindLastAndNextNohupFileName(${pathNameToSearch}) ;
    return ${nextName} ;
}

sub FindLastNohupFileName
{
    my $pathNameToSearch = $_[0] ;
    my ($lastName , $nextName) = FindLastAndNextNohupFileName(${pathNameToSearch}) ;
    return ${lastName} ;
}

sub FindLastAndNextNohupFileName
{
    my $pathNameToSearch = $_[0] ;
    my $maxNohupNumber = `ls ${pathNameToSearch}/nohup.out* | sed 's/^.*nohup\.out\.//g' | sort -n | tail -1` ;
    print "\nmaxNohupNumber found is : ${maxNohupNumber}" ;
    my $lastNohupFileNameToReturn = "nohup.out." . ${maxNohupNumber} ;
    chomp ${lastNohupFileNameToReturn} ;
    ## Chomp is not needed for the next nohup file name, because it looks like arithmatic operation removed the newline
    my $nextNohupFileNameToReturn = "nohup.out." . (${maxNohupNumber} + 1) ;
    return (${lastNohupFileNameToReturn}, ${nextNohupFileNameToReturn}) ;
}

sub CheckFileHandlingModuleLoaded
{
    print "\nThe module FileHandling has been loaded successfully." ;
}

666 ;
