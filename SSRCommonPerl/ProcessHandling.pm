#!/usr/bin/perl

use strict ;
use warnings ;

package SSRPerlScripts::CommonPerl ;

sub GetProcessPid
{
    my ($processIdentifierText) = @_ ;
    my $output = `ps -ef | grep $processIdentifierText | grep -v grep | head -1` ;
    print "\n\nThe pid output obtained is : \n${output}\n" ;
    my $pidvar = $output ;
    $pidvar =~ s/^[^ \t]*[ \t]*([^ \t]*)[^\n]*/\1/g ;
    chomp ${pidvar} ;
    print "\nThe pid obtained is : ${pidvar}\n" ;
    return $pidvar ;
}

sub KillProcess
{
    my $processStringToKill = $_[0] ;
    my $pidToKill = GetProcessPid(${processStringToKill}) ;
    print "\nThe processString to kill was : ${processStringToKill}\nThe processIdToKill is : ${pidToKill}\n" ;
    `kill -9 ${pidToKill}` ;
    my $pidAfterKill = GetProcessPid(${processStringToKill}) ;
    print "\npidAfterKill is : ${pidAfterKill}" ;
    no warnings ;
    if($pidAfterKill == "")
    {
        print "\nProcess to kill was killed successfully." ;
    }
    else
    {
        print "\nProcess to kill could not be killed." ;
    }
    use warnings ;
}

sub CheckProcessHandlingModuleLoaded
{
    print "\nThe module ProcessHandling has been loaded successfully.\n" ;
}

666 ; ## This seems to be here because there has to be some code to execeute apparently
